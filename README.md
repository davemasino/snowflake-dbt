## dbt Demo Example

---
- [What is dbt](https://dbt.readme.io/docs/overview)?
- Read the [dbt viewpoint](https://dbt.readme.io/docs/viewpoint)
- [Installation](https://dbt.readme.io/docs/installation)
- Join the [chat](http://ac-slackin.herokuapp.com/) on Slack for live questions and support.

---
#### Demo Prep

Create Snowflake raw table and COPY in Flights data

```bash
create table public.flights_raw
(
    src variant
);
     
copy into public.flights_raw
from @greatlakes_datasets/flights/
  file_format = (
  type = 'PARQUET'
);
```

#### Next ... 


