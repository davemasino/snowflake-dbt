{{ 
    config(
        materialized='incremental'
    ) 
}}

select
  year 
  ,quarter 
  ,month 
  ,dayofmonth
  ,flightdate
  ,uniquecarrier 
  ,airlineid 
  ,carrier 
  ,originairportid 
  ,originairportseqid 
  ,origincitymarketid 
  ,origin 
  ,origincityname 
  ,originstate 
  ,originstatefips 
  ,originstatename 
  ,originwac 
  ,destairportid 
  ,destairportseqid 
  ,destcitymarketid 
  ,dest 
  ,destcityname 
  ,deststate 
  ,deststatefips 
  ,deststatename 
  ,destwac 
  --Measures
  ,sum(depdelay)               as depdelay
  ,sum(depdelayminutes)        as depdelayminutes
  ,sum(depdel15)               as depdel15
  ,sum(taxiout)                as taxiout
  ,sum(taxiin)                 as taxiin
  ,sum(arrdelay)               as arrdelay
  ,sum(arrdelayminutes)        as arrdelayminutes
  ,sum(arrdel15)               as arrdel15
  ,sum(cancelled)              as cancelled
  ,sum(diverted)               as diverted
  ,sum(crselapsedtime)         as crselapsedtime
  ,sum(actualelapsedtime)      as actualelapsedtime
  ,sum(airtime)                as airtime
  ,sum(flights)                as flights
  ,sum(distance)               as distance
  ,sum(carrierdelay)           as carrierdelay
  ,sum(weatherdelay)           as weatherdelay
  ,sum(nasdelay)               as nasdelay
  ,sum(securitydelay)          as securitydelay
  ,sum(lateaircraftdelay)      as lateaircraftdelay
  ,sum(totaladdgtime)          as totaladdgtime
  ,sum(longestaddgtime)        as longestaddgtime
from flights
{% if is_incremental() %}

-- this filter will only be applied on an incremental run
where flightdate > (select max(flightdate) from {{ this }})

{% endif %}
group by 
  year 
  ,quarter 
  ,month 
  ,dayofmonth
  ,flightdate
  ,uniquecarrier 
  ,airlineid 
  ,carrier 
  ,originairportid 
  ,originairportseqid 
  ,origincitymarketid 
  ,origin 
  ,origincityname 
  ,originstate 
  ,originstatefips 
  ,originstatename 
  ,originwac 
  ,destairportid 
  ,destairportseqid 
  ,destcitymarketid 
  ,dest 
  ,destcityname 
  ,deststate 
  ,deststatefips 
  ,deststatename 
  ,destwac 

